#!/bin/sh

set -e

TARBALL=archlinux-bootstrap-2017.04.01-x86_64.tar.gz
wget -c https://mirrors.kernel.org/archlinux/iso/latest/$TARBALL

tar xvf $TARBALL

tmp=archtmp

tar -C  root.x86_64 -c . | docker import - "$tmp"

docker build -t archlinux .

docker rmi "$tmp"
sudo rm -rf root.x86_64
